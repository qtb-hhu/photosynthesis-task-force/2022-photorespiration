# Project template

This template has three folders: code, literature and paper.

- `/code`: models, software, data analysis
- `/literature`: summaries of important papers or other interesting scientific reading
- `/paper`: latex source for the final paper

Please do add citations to your literature, it's no fun searching for this stuff after the fact ;)

There is some continuous integration defined to build the paper. This code is currently commented out, remove those comments once you are at the writing stage.

## Setting up

- `pip install pre-commit`
- `pre-commit install`
- `pip install -r code/requirements.txt`
