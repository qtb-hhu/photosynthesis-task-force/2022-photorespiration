# Code

- Write dependencies in `requirements.in` file
- Use `pip-compile --generate-hashes requirements.in` to generate `requirements.txt`
- Use `pip install -r requirements.txt` to install dependencies as usual
- Use `pip-compile --upgrade --generate-hashes  --allow-unsafe` to update dependencies
